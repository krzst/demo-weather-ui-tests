package com.demo.weather.module;

import junit.framework.TestCase;
import org.apache.commons.logging.Log;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.math.BigInteger;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MainTest  extends TestCase {
    public static final String ACCOUNT_HASH = "aa2cc9ed0b421b9a01944035d1697559:5e7b74ee756d6ab9d628af2d6cb24639";
    public static final String HUB_URL = "http://" + ACCOUNT_HASH + "@hub.testingbot.com/wd/hub";

    public static final String INIT_CITY = "Kielce";

    private RemoteWebDriver driver;

    @Before
    public void setUp() throws Exception {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platform", "WIN10");
        capabilities.setCapability("version", "dev");
        capabilities.setCapability("browserName", "chrome");
        capabilities.setCapability("name", "DemoWeather UI (" + getName() + ") " + new Date());
        capabilities.setCapability("public", true);

        this.driver = new RemoteWebDriver(new URL(HUB_URL), capabilities);
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);

        this.driver.get("https://kris-demo-weather.herokuapp.com/");
    }

    @Test
    public void testPageTitle() throws Exception {
        assertEquals("DemoWeather", driver.getTitle());
    }

    @Test
    public void testInitCity() throws Exception {
        WebElement cityHeader = driver.findElementByClassName("city-header");
        assertTrue(cityHeader.getText().contains(INIT_CITY));
    }

    @Test
    public void testChangeCity() throws Exception {
        // ARRANGE
        String newCityName = "London";

        WebElement searchButton = driver.findElementById("search-button");
        WebElement cityInput = driver.findElementById("city-input");

        // ACT
        cityInput.clear();
        cityInput.sendKeys(newCityName);
        searchButton.click();

        // CHECK
        WebElement cityHeader = driver.findElementByClassName("city-header");
        assertTrue(cityHeader.getText().contains(newCityName));
    }

    @Test
    public void testIfTemperatureDisplayed() throws Exception {
        // ACT
        WebElement currentTemperatureBox = driver.findElementByClassName("weather-daily-temperature");
        WebElement temperatureValue = currentTemperatureBox.findElement(By.className("weather-value"));
        WebElement temperatureUnit = currentTemperatureBox.findElement(By.className("weather-unit"));

        // TEST
        String temperatureValueAsString = temperatureValue.getText();
        assertNotNull("Temperature is null", temperatureValueAsString);
        assertTrue("Temperature is empty", !temperatureValueAsString.equals(""));

        boolean isTemperatureNumeric = temperatureValueAsString.chars().allMatch( Character::isDigit );
        assertTrue("Temperature has to be numeric value", isTemperatureNumeric);

        assertEquals("Temperature unit incorrect", "°C", temperatureUnit.getText());
    }

    @Test
    public void testPickedDefaultDateShouldBeFirstDayAndFirstHour() {
        WebElement pickedDateInfo = driver.findElementById("picked-date");
        List<WebElement> calendarDays = driver.findElementsByClassName("weather-day");
        List<WebElement> hours = driver.findElementsByClassName("weather-by-hour");

        assertTrue("No calendar dates found", !calendarDays.isEmpty());
        assertTrue("No hours found", !hours.isEmpty());

        // by default the weather should be for very first day
        WebElement calendarDay = calendarDays.get(0);
        String month = calendarDay.findElement(By.className("weather-calendar-month")).getText();
        String day = calendarDay.findElement(By.className("weather-calendar-day")).getText();

        // by default the weather should be for first available hour for given day
        String hour = hours.get(0).getText();

        String pickedDateInfoString = pickedDateInfo.getText();
        String expectedPickedDay = "(" + month + " " + day + " " + hour +")";

        assertEquals("Incorrect date in info box", expectedPickedDay, pickedDateInfoString);
    }

    @After
    public void tearDown() throws Exception {
        String reportUrl = getReportUrl();
        System.out.println("Test (" + getName() + ") result can be find here: " + reportUrl);

        this.driver.quit();
    }

    private String getReportUrl() throws NoSuchAlgorithmException {
        String sessionId = driver.getSessionId().toString();

        MessageDigest m = MessageDigest.getInstance("MD5");
        String s = ACCOUNT_HASH + ":" + sessionId;
        m.update(s.getBytes(),0, s.length());
        String authHash = new BigInteger(1, m.digest()).toString(16);

        String reportUrl = "https://testingbot.com/tests/" + sessionId + "?auth=" + authHash;

        return reportUrl;
    }
}
